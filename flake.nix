{
  description = "Skynet Almnui Renew";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "skynet-website-renew";
        src = self;
        installPhase = "mkdir -p $out; cp -R src/* $out";
      };
    }
  );
}