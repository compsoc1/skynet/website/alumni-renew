# skynet.ie-renew
Webpage explaining membership options to Alumni members.  

## Previous version
This repo originally lived at <https://github.com/ULCompSoc/skynet.ie-renew>.  
It lived at <https://skynet.ie/renew>.  
It used a cron job and [this script][1] to keep it updated.

## Current version 
Now it is living at <https://renew.skynet.ie> with a pipeline to automatically update it.





[1]: https://gitlab.skynet.ie/compsoc1/skynet/website/alumni-renew/-/blob/5d1dd98bf6cc33be275c28f8fb6871fb10e8f687/update-renew-from-github.sh